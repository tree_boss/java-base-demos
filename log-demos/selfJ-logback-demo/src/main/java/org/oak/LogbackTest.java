package org.oak;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * org.oak.LogbackTest
 *
 * @author : Oak
 * @version : V1.0
 * @since :  1/10/24 9:53 PM
 */
public class LogbackTest {
    static Logger log = LoggerFactory.getLogger(LogbackTest.class);

    public static void main(String[] args) {
        log.info("info");
        log.debug("debug");
        log.warn("warn");
        log.error("error");
        log.trace("trace");
        info(args);
    }

    public static void info(String... args) {
        System.out.println(args);
    }
}
