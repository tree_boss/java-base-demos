package org.oak;

/**
 * org.oak.ThreadTest
 *
 * @author : Oak
 * @version : V1.0
 * @since :  1/11/24 8:05 PM
 */
public class ThreadTest {
    public static void main(String[] args) {

        Thread thread = new Thread(() -> {
            System.out.println(Thread.currentThread().getName());
        });

        System.out.println("main");

        Object o = new Object();
        System.out.println(o);
        System.out.println(o);
        thread.start();
        System.out.println("age");

        System.out.println("name");
        System.out.println("age");

        System.out.println("aaa");
    }
}
